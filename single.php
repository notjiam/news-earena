<?php
/**
 * The template for displaying all single posts.
 *
 * @package ea
 */

get_header(); ?>
<?php 
	$thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' ); 
?>
	<div class="content-header --content" style="<?php if(!empty($thumb_url[0])):?> background-image:url('<?= $thumb_url[0] ?>'); <?php endif;?>">
		<div class="image" style="<?php if(!empty($thumb_url[0])):?> background-image:url('<?= $thumb_url[0] ?>'); <?php endif;?>"></div>
	</div>

	<div class="container">
		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' ); ?>

		<?php endwhile; // End of the loop. ?>

	</div>

<?php get_footer(); ?>
