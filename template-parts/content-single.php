<?php
/**
 * Template part for displaying single posts.
 *
 * @package ea
 */

?>

<?php 
	$view_count = get_post_meta($post->ID, 'view_count', true);
	$categories = get_the_category(); 
	$categories_str = '';
	$view_count = empty($view_count) ? 1 : $view_count+1;
	update_post_meta( $post->ID, 'view_count', $view_count );
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<div class="category">
			<span class="view-count badge badge-primary badge-pill"><i class="far fa-eye"></i> <?= !empty($view_count) ? number_format( (int)$view_count ) : 0 ?></span>
			
			<?php if(!empty($categories)): ?>
				<?php 
					foreach($categories as $category):
						$categories_str .= $category->term_id.',';
					?>
					<a class="badge badge-outline badge-primary badge-pill" href="<?= ea_category_link($category->slug) ?>"><?= $category->name ?></a>
				<?php endforeach;?>
			<?php endif;?>
		</div>
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php ea_posted_on(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>

		<div class="tags-wrapper">
			<?php $tags_list = get_the_tag_list( '', '' );?>
			<?= $tags_list ?>
		</div>

		<div class="share-wrapper">
			<h3 class="title">Share this article</h3>
			<a class="line" href="https://lineit.line.me/share/ui?url=<?php echo get_permalink( );?>&title=<?php echo get_the_title(); ?>" target="_blank"></a>
			<a class="twitter" href="http://twitter.com/intent/tweet?status=<?php the_permalink() ?>+<?php the_title() ?>" target="_blank"></a>
			<a class="facebook" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink() ?>&title=<?php the_title() ?>" target="_blank"></a>
		</div>

		<?php  
		$args_relate = array(
				'orderby' => 'rand',
				'posts_per_page' => 2,
				'cat' => $categories_str,
			);
			$custom_query_relate = new WP_Query($args_relate);
			if( $custom_query_relate->have_posts() ): ?>
				<div class="relate-wrapper">
					<div class="card-deck card-deck-2-item">
					<?php
						while( $custom_query_relate->have_posts() ): $custom_query_relate->the_post(); 
							get_template_part( 'template-parts/card-item' );
						endwhile;
					?>
					</div>
				</div>
			<?php endif;
			wp_reset_postdata();
		?>
	</div><!-- .entry-content -->

	<section class="section-main-other-wrapper section-wrapper">
		<h2 class="title">Other</h2>
		<?php $categories = ea_get_categories_data() ?>
		<div class="card-deck-6-item">
			<?php foreach($categories as $category): ?>
				<?php include(locate_template('template-parts/card-category.php')); ?>
			<?php endforeach; ?>
		</div>
	</section>

</article><!-- #post-## -->

