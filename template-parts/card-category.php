<div class="card-item --category">
    <div class="card-container">
        <a href="<?= ea_category_link($category['slug']) ?>" class="card-top"  title="<?= $category['name'] ?>" >
            <div class="card-image-top" style="background-image:url('<?= $category['image'] ?>')"></div>
        </a>
        <div class="card-detail">
            <h3 class="title"><a href="<?= ea_category_link($category['slug']) ?>" title="<?= $category['name'] ?>"><?= $category['name'] ?></a></h3>
        </div>
    </div>
</div>