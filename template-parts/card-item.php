<div class="card-item">
    <div class="card-container">
        <a href="<?php the_permalink() ?>" class="card-top"  title="<?php the_title(); ?>" >
            <?php 
                $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' ); 
                $view_count = get_post_meta($post->ID, 'view_count', true);
                $categories = get_the_category();
            ?> 
            <div class="card-image-top" style="background-image:url('<?= $thumb_url[0] ?>')"></div>
            <div class="card-content-top">
                <span class="view-count"><i class="far fa-eye"></i> <?= !empty($view_count) ? number_format( (int)$view_count ) : 0 ?></span>
            </div>
        </a>
        <div class="card-detail">
            <div class="category">
                <?php if(!empty($categories)): ?>
                    <?php foreach($categories as $category):?>
                        <a class="badge badge-outline badge-primary badge-pill" href="<?= ea_category_link($category->slug) ?>"><?= $category->name ?></a>
                    <?php endforeach;?>
                <?php endif;?>
            </div>
            <h3 class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
            <span class="meta-date">
                <i class="far fa-clock"></i>
                <?= ea_get_post_date(); ?>
            </span>
        </div>
    </div>
</div>