<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ea
 */

$catrgory = get_queried_object();
$catrgory_slug = '';
if( !empty($catrgory) && $catrgory->taxonomy == 'category' ):
	$catrgory_slug = ea_get_catrgory_slug($catrgory->slug);
endif;
get_header(); ?>

	<?php if( !empty($catrgory) && $catrgory->taxonomy == 'category' ): ?>
		<header class="content-header --category <?= $catrgory_slug; ?>">
			<h1 class="title"><?= $catrgory->name; ?></h1>
		</header><!-- .page-header -->
		<section class="search-section-wrapper">
			<?= get_search_form(); ?>
		</section>
		<?php elseif( !empty($catrgory) && $catrgory->taxonomy == 'post_tag' ):?>
			<header class="page-header">
				<h1 class="title">#<?= $catrgory->name ?></h1>
			</header>
		<?php else:?>
			<header class="page-header">
				<?php
					the_archive_title( '<h1 class="page-title">', '</h1>' );
					the_archive_description( '<div class="taxonomy-description">', '</div>' );
				?>
			</header><!-- .page-header -->
		<?php endif; ?>

	<?php if ( have_posts() ) : ?>
		
		<div class="card-deck card-deck-3-item">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					get_template_part( 'template-parts/card-item' );
				?>
			<?php endwhile; ?>
		</div>

		<?php ea_pagination(); ?>

	<?php else : ?>
		<?php get_template_part( 'template-parts/content', 'none' ); ?>
	<?php endif; ?>

	<section class="section-main-other-wrapper section-wrapper">
		<h2 class="title">Other</h2>
		<?php $categories = ea_get_categories_data() ?>
		<div class="card-deck-6-item">
			<?php foreach($categories as $category): ?>
				<?php include(locate_template('template-parts/card-category.php')); ?>
			<?php endforeach; ?>
		</div>
	</section>

<?php get_footer(); ?>
