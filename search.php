<?php
/**
 * The template for displaying search results pages.
 *
 * @package ea
 */

get_header(); ?>

	<header class="page-header">
		<h1 class="title"><?php printf( esc_html__( 'Search Results for: %s', 'ea' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
	</header><!-- .page-header -->
	<section class="search-section-wrapper">
		<?= get_search_form(); ?>
	</section>
	<?php if ( have_posts() ) : ?>
		
		<div class="card-deck card-deck-3-item">
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
					get_template_part( 'template-parts/card-item' );
				?>
			<?php endwhile; ?>
		</div>

		<?php ea_pagination(); ?>

	<?php else : ?>
		<?php get_template_part( 'template-parts/content', 'none' ); ?>
	<?php endif; ?>
	
	<section class="section-main-other-wrapper section-wrapper">
		<h2 class="title">Other</h2>
		<?php $categories = ea_get_categories_data() ?>
		<div class="card-deck-6-item">
			<?php foreach($categories as $category): ?>
				<?php include(locate_template('template-parts/card-category.php')); ?>
			<?php endforeach; ?>
		</div>
	</section>

<?php get_footer(); ?>
