import '../sass/style.scss'
import './vendor/skip-link-focus-fix'

import './util/go-to-top'
import './util/animate-hash'

import './components/main-nav'

import $ from 'jquery'
import 'bootstrap/js/dist/util'

import './pages/main'