import $ from 'jquery';

$('[data-as-open-link]').click(e => {
    e.preventDefault();
    const url = $(this).attr('data-as-open-link');
    if( url && url !== '#' ){
        window.location.href = url;
    }
});