import $ from 'jquery';

$('[data-as-go-to-top]').click(()=>{
    $('body,html').animate({scrollTop: 0}, $(window).scrollTop()/3, 'swing');
})