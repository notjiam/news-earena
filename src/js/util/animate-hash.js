import $ from 'jquery';

$(document).on('click', 'a[href^="#"]', event => {
    event.preventDefault();
    if( $.attr(this, 'href') === '#' ){
        return;
    }
    $('html, body').animate({
        scrollTop: $($.attr(this, 'href')).offset().top - 100
    }, 1000,'swing');
});