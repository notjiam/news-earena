import $ from 'jquery'
import Swiper from 'swiper/dist/js/swiper.js'

$(document).ready(()=>{
    const swiper = new Swiper('.feature-swiper', {
        loop: true,
        autoplay:{
            delay: 5000,
        },
        pagination: {
            el: '.feature-swiper .swiper-pagination',
            type: 'bullets',
        },
    })
    
})