import $ from 'jquery';

const $body = $('body');
const $mainNav = $('.main-navigation');

const stickyNavTop = 150;

$(window).scroll(function () {
    stickyNav();
});
const stickyNav = () => {
    let scrollTop = $(window).scrollTop();
    if (scrollTop > stickyNavTop) {
        if (!$body.hasClass('sticky-out')) {
            $body.addClass('sticky-header');
        }
    } else {
        removeStickyNav($body);
    }
}
const removeStickyNav = ($mainNav) => {
    if (!$body.hasClass('sticky-header') || $body.hasClass('sticky-header-out') || $('body').hasClass('nav-active')) {
        return;
    }
    $body.removeClass('sticky-header');
}

const $menuToggle = $mainNav.find('.menu-toggle');
$menuToggle.click(() => {
    if ($body.hasClass('nav-active')) {
        const pos = parseInt($body.css('top')) * -1;
        $body.css('top', '');
        $body.removeClass('nav-active');
        $(window).scrollTop(pos);
    } else {
        const scrollTop = -$(window).scrollTop();
        $body.addClass('nav-active');
        $body.css('top', scrollTop);
    }
});

$mainNav.find('li.menu-item-has-children > a').click(e => {
    if( $(window).width() < 768 ){
        $(e.currentTarget).closest('li').toggleClass('open');
        e.preventDefault();
    }
});