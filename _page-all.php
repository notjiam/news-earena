<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package ea
 */

get_header(); ?>

	
	<header class="content-header --category all">
		<h1 class="title"><?= $catrgory->name; ?></h1>
	</header><!-- .page-header -->
	<section class="search-section-wrapper">
		<?= get_search_form(); ?>
	</section>

	<?php 
		$args = array(
			'post_type' => 'post',
		);
		$custom_query = new WP_Query($args);
		if( $custom_query->have_posts() ):
			while( $custom_query->have_posts() ): $custom_query->the_post(); 
			?>
				<div class="card-deck card-deck-3-item">
					<?php
						get_template_part( 'template-parts/card-item' );
					?>
				</div>
				<?php ea_pagination(); ?>
			<?php
			endwhile;
		else:
		endif;
		wp_reset_postdata();
	?>


	<?php if ( have_posts() ) : ?>
		
		

	<?php else : ?>
		<?php get_template_part( 'template-parts/content', 'none' ); ?>
	<?php endif; ?>

	<section class="section-main-other-wrapper section-wrapper">
		<h2 class="title">Other</h2>
		<?php $categories = ea_get_categories_data() ?>
		<div class="card-deck-6-item">
			<?php foreach($categories as $category): ?>
				<?php include(locate_template('template-parts/card-category.php')); ?>
			<?php endforeach; ?>
		</div>
	</section>

<?php get_footer(); ?>
