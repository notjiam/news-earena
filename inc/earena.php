<?php

define('EA_MAIN_URL', 'https://earena.com');


function ea_get_categories_data(){
    $categories = array(
        "earena-news" => array(
            "name" => "eArena News",
            "slug" => "earena-news",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-earena-news@2x.jpg"
        ),
        "esport" => array(
            "name" => "eSport",
            "slug" => "esport",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-esport@2x.jpg"
        ),
        "luckydraw-enjoyment" => array(
            "name" => "Luckydraw / Enjoyment",
            "slug" => "luckydraw-enjoyment",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-luckydraw-enjoyment@2x.jpg"
        ),
        "news" => array(
            "name" => "News",
            "slug" => "news",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-news@2x.jpg"
        ),
        "review" => array(
            "name" => "Review",
            "slug" => "review",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-review@2x.jpg"
        ),
        "tip-trick" => array(
            "name" => "Tip / Trick",
            "slug" => "tip-trick",
            "image" => get_bloginfo('stylesheet_directory')."/assets/images/category/thumb-tip-trick@2x.jpg"
        )
    );
    return $categories;
}

// Numbered Pagination
if ( !function_exists( 'ea_pagination' ) ) {
	
	function ea_pagination() {
		
		$prev_arrow = is_rtl() ? '<i class="fas fa-chevron-right"></i>' : '<i class="fas fa-chevron-left"></i>';
		$next_arrow = is_rtl() ? '<i class="fas fa-chevron-left"></i>' : '<i class="fas fa-chevron-right"></i>';
		
		global $wp_query;
		$total = $wp_query->max_num_pages;
		$big = 999999999; // need an unlikely integer
		if( $total > 1 )  {
			 if( !$current_page = get_query_var('paged') )
				 $current_page = 1;
			 if( get_option('permalink_structure') ) {
				 $format = 'page/%#%/';
			 } else {
				 $format = '&paged=%#%';
			 }
			echo paginate_links(array(
				'base'			=> str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format'		=> $format,
				'current'		=> max( 1, get_query_var('paged') ),
				'total' 		=> $total,
				'mid_size'		=> 3,
				'type' 			=> 'list',
				'prev_text'		=> $prev_arrow,
				'next_text'		=> $next_arrow,
			 ) );
		}
	}
	
}

function ea_filter_post_tag_term_links( $term_links ) {
    $wrapped_term_links = array();
    foreach ( $term_links as $term_link ) {
        $wrapped_term_links[] = str_replace('rel="tag">', 'rel="tag">#', $term_link);
    }
    return $wrapped_term_links;
}
add_filter( 'term_links-post_tag', 'ea_filter_post_tag_term_links' );


if (!is_admin()) {
    function ea_search_filter($query) {
    if ($query->is_search) {
        $query->set('post_type', 'post');
    }
        return $query;
    }
    add_filter('pre_get_posts','ea_search_filter');
}


function ea_t($text_th, $text_en ) {
    if( function_exists('pll_current_language') ):
        return pll_current_language() == 'th' ? $text_th : $text_en;
    else:
        return $text_th;
    endif;
}

function ea_category_link( $slug ){

    if( function_exists('pll_current_language') ):
        if( pll_current_language() == 'th' ):
            return home_url().'/category/'.$slug;
        else:
            return home_url().'/category/'.$slug.'-en';
        endif;
    else:
        return home_url().'/category/'.$slug;
    endif;
}

function ea_get_catrgory_slug( $slug ) {
    if( function_exists('pll_current_language') ):
        return preg_replace( '/-en$/', '', $slug);
    else:
        return $slug;
    endif;
}