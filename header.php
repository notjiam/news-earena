<?php
/**
 * @package ea
 */

?><!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo('pingback_url');?>">
	<link rel="shortcut icon" href="<?= get_bloginfo('stylesheet_directory')?>/assets/images/favicon.ico" type="image/x-icon" class="jsx-3088832098 next-head">
	<link rel="icon" href="<?= get_bloginfo('stylesheet_directory')?>/assets/images/favicon.ico" type="image/x-icon" class="jsx-3088832098 next-head">
	<?php wp_head();?>
</head>

<body <?php body_class();?>>
<ul class="lang-nav-mobile">
	<?php if( function_exists('pll_the_languages') ):
		pll_the_languages(array(
			'hide_current' => 1
		));
	endif; ?>
</ul>
<?php get_template_part( 'template-parts/social-link' ); ?>
<div class="site-wrapper">
	<div class="site-row row no-gutters">
		<div class="site-header col-12 col-lg-2 order-2 order-lg-1 shadow-sm bg-white col-bottom">
			<nav class="main-nav">
				<a class="d-none d-lg-block " href="<?= EA_MAIN_URL ?>"><img src="<?= get_bloginfo('stylesheet_directory') ?>/assets/images/share/logo-eArena_black_beta.png" srcset="<?= get_bloginfo('stylesheet_directory') ?>/assets/images/share/logo-eArena_black_beta@2x.png 2x" alt="eArena" class="logo img-fluid"></a>
				<a class="link tournament" href="<?= EA_MAIN_URL ?>/tournament/list"><?= ea_t('การแข่งขัน','Tournaments') ?></a>
				<a class="link team" href="<?= EA_MAIN_URL ?>/team/list"><?= ea_t('ทีม','Team') ?></a>
				<a class="link profile" href="<?= EA_MAIN_URL ?>/user/list"><?= ea_t('ผู้เล่น','Profile') ?></a>
				<a class="link news is-active" href="<?= home_url() ?>"><?= ea_t('ข่าวสาร','News') ?></a>
				<a class="link thailand-earena" href="https://thailand.earena.com"><span class="d-none d-lg-inline">Thailand Esports Arena</span><span class="d-inline d-lg-none">THEA</span></a>
			</nav>
			<ul class="lang-nav">
				<?php if( function_exists('pll_the_languages') ):
					// pll_the_languages(array(
					// 	'hide_current' => 1
					// ));
				endif; ?>
			</ul>
		</div><!-- .site-header -->
	<div class="col-12 col-lg-10 order-1 order-lg-2">
		<div class="section-container px-2 py-4">
			<div class="container-fluid">
		
	
