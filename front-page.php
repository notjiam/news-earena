<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package ea
 */

get_header(); ?>
	<div class="content-header padding-none">
		<?php 
			$sticky = get_option('sticky_posts');
			if (!empty($sticky)) {
				rsort($sticky);
				$args = array(
					'post__in' => $sticky
				);
				$custom_query = new WP_Query($args);
				if( $custom_query->have_posts() ): ?>
					<div class="swiper-container feature-swiper">
						<div class="swiper-wrapper">
							<?php while( $custom_query->have_posts() ): $custom_query->the_post(); ?>	
								<?php $thumb_url = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'full' ); ?>
								<div class="swiper-slide" style="<?php if(!empty($thumb_url[0])):?> background-image:url('<?= $thumb_url[0] ?>'); <?php endif;?>">
									<div class="image" style="<?php if(!empty($thumb_url[0])):?> background-image:url('<?= $thumb_url[0] ?>'); <?php endif;?>"></div>
									<div class="item-detail">
										<?php 
											$categories = get_the_category(); 
											$view_count = get_post_meta($post->ID, 'view_count', true);
										?>
										<div class="category">
											<span class="view-count badge badge-primary badge-pill"><i class="far fa-eye"></i> <?= !empty($view_count) ? number_format( (int)$view_count ) : 0 ?></span>
											<?php if(!empty($categories)): ?>
												<?php foreach($categories as $category):?>
													<a class="badge badge-outline badge-primary badge-pill" href="<?= ea_category_link($category->slug) ?>"><?= $category->name ?></a>
												<?php endforeach;?>
											<?php endif;?>
										</div>
										
										
										<h3 class="title"><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
										<span class="meta-date">
											<i class="far fa-clock"></i>
											<?= ea_get_post_date(); ?>
										</span>
									</div>
								</div>
							<?php endwhile; ?>
						</div>
						<div class="swiper-pagination"></div>
					</div>
				<?php 
				endif;
				wp_reset_postdata();
			}
		?>
	</div>

	<section class="search-section-wrapper">
		<?= get_search_form(); ?>
	</section>
	<section class="section-main-popular-wrapper section-wrapper">
		<div class="row">
			<div class="col-12 col-md-6">
				<h2 class="title">
					<?= ea_t('ข่าวล่าสุด','Latest') ?>
				</h2>
				<div class="card-deck">
					<?php 
						$args_lastes = array(
							'post_type' => 'post',
							'posts_per_page' => 1,
							'ignore_sticky_posts' => 1,
						);
						$custom_query_lastes = new WP_Query($args_lastes);
						if( $custom_query_lastes->have_posts() ):
							while( $custom_query_lastes->have_posts() ): $custom_query_lastes->the_post(); 
								get_template_part( 'template-parts/card-item' );
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
			</div>
			<div class="col-12 col-md-6">
				<h2 class="title">
					<?= ea_t('ข่าวยอดนิยม','Popular') ?>
				</h2>
				<div class="card-deck card-deck-2-item">
					<?php 
						$args_popular = array(
							'post_type' => 'post',
							'meta_key' => 'view_count',
							'orderby' => 'meta_value_num',
							'order' => 'DESC',
							'posts_per_page' => 4,
							'ignore_sticky_posts' => 1,
						);
						$custom_query_popular = new WP_Query($args_popular);
						if( $custom_query_popular->have_posts() ):
							while( $custom_query_popular->have_posts() ): $custom_query_popular->the_post(); 
								get_template_part( 'template-parts/card-item' );
							endwhile;
						endif;
						wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
		
	</section>

	<section class="section-main-all-news-wrapper section-wrapper">
		<h2 class="title">
			<?= ea_t('ข่าวทั้งหมด','All News') ?>
		</h2>
		<div class="card-deck card-deck-3-item">
			<?php 
				$args_all = array(
					'post_type' => 'post',
					'post_per_page' => 6
				);
				$custom_query_all = new WP_Query($args_all);
				if( $custom_query_all->have_posts() ):
					while( $custom_query_all->have_posts() ): $custom_query_all->the_post(); 
						get_template_part( 'template-parts/card-item' );
					endwhile;
				endif;
				wp_reset_postdata();
			?>
		</div>
		<div class="action text-center">
			<a href="<?= home_url(); ?>/all/" class="btn btn-primary btn-lg btn-pill" title="View more">
				<?= ea_t('ดูทั้งหมด','View more') ?>
			</a>
		</div>
	</section>

	<section class="section-main-other-wrapper section-wrapper">
		<h2 class="title">Other</h2>
		<?php $categories = ea_get_categories_data() ?>
		<div class="card-deck-6-item">
			<?php foreach($categories as $category): ?>
				<?php include(locate_template('template-parts/card-category.php')); ?>
			<?php endforeach; ?>
		</div>
	</section>

<?php get_footer(); ?>
