<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package ea
 */

get_header(); ?>

	<div id="primary" class="content-area page-404">
		<article <?php post_class('section-single'); ?>>
			<header class="entry-header">
				<div class="entry-header-container container">
					<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'ea' ); ?></h1>
					<div class="page-content">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'ea' ); ?></p>
						<section class="search-section-wrapper">
							<?= get_search_form(); ?>
						</section>
					</div>
				</div>
			</header><!-- .entry-header -->
		</article><!-- #post-## -->
	<div>

<?php get_footer(); ?>
