const path = require("path");
const BrowserSyncPlugin = require("browser-sync-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = [{
    entry: "./src/js/script.js",
    output: {
      path: path.resolve(__dirname, "assets/js/"),
      filename: "script.js"
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.scss$/,
          use: [{
            loader: 'style-loader'
          }, 
          {
            loader: MiniCssExtractPlugin.loader
          },
          {
            loader: 'css-loader?url=false'
          }, {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          }, {
            loader: 'sass-loader'
          }]
        }
      ]
    },
    optimization: {
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          sourceMap: true
        }),
        new OptimizeCSSAssetsPlugin({})
      ]
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: '../css/style.css',
      }),
      new BrowserSyncPlugin({
        host: "localhost",
        port: 5000,
        files: [
          "./src/js/**/*.js",
          "./src/sass/**/*.scss"
        ],
        proxy: "http://localhost:80"
      })
    ]
  },
];
