<?php
/**
 * ea functions and definitions
 *
 * @package ea
 */

define('THEME_VERSION', '1.0.7' );
define('THEME_API_NONCE', 'ea_api_nonce' );

if ( ! function_exists( 'ea_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function ea_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on ea, use a find and replace
	 * to change 'ea' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'ea', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we ealare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'main-menu' => esc_html__( 'Main Menu', 'ea' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'ea_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // ea_setup
add_action( 'after_setup_theme', 'ea_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function ea_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'ea_content_width', 640 );
}
add_action( 'after_setup_theme', 'ea_content_width', 0 );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function ea_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'ea' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'ea_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function ea_scripts() {
	/**
	 * Remove wp-embed.min.js
	 */
	wp_deregister_script( 'wp-embed' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_style( 'ea-style', get_stylesheet_directory_uri() .  '/assets/css/style.css', array(), THEME_VERSION );

	wp_enqueue_script( 'ea-script', get_stylesheet_directory_uri() .  '/assets/js/script.js', array(), THEME_VERSION, true );

    wp_localize_script( 'ea-script', 'site_params', array( 
        'base_url' => site_url(),
        'ajax_url' => admin_url( 'admin-ajax.php' ),
        'wpnonce' => wp_create_nonce( THEME_API_NONCE )
    ) );

	
}
add_action( 'wp_enqueue_scripts', 'ea_scripts' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Remove wp-emoji-release.min.js
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

require get_template_directory() . '/inc/earena.php';
